package com.mercadolibre.mercadopago.melichallenge.mvp.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mercadolibre.mercadopago.melichallenge.R
import com.mercadolibre.mercadopago.melichallenge.helpers.CustomMessage
import com.mercadolibre.mercadopago.melichallenge.mvp.presenter.EnterAmountPresenter
import com.mercadolibre.mercadopago.melichallenge.mvp.view.viewInterface.EnterAmountView
import kotlinx.android.synthetic.main.fragment_amount.*


class EnterAmountFragment: Fragment(), EnterAmountView, View.OnClickListener  {
    val TAG: String = "EnterAmountFragment"

    private lateinit var mPresenter: EnterAmountPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_amount, container, false)
        this.mPresenter = EnterAmountPresenter(this)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button_accept.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.button_accept -> {
                mPresenter.validateAmount(edit_amount.text.toString(),context!!)
            }
        }
    }

    override fun showError(code: Int, message: String) {
        CustomMessage.handlerRequestError(activity!!, code, message)
    }

    override fun showView() {
        val fragmentTransaction = fragmentManager!!.beginTransaction()

        val bundle = Bundle()
        bundle.putString(getString(R.string.bundle_amount), edit_amount.text.toString())

        val paymentMethodFragment = SelectPaymentMethodFragment()
        paymentMethodFragment.arguments = bundle

        fragmentTransaction.replace(R.id.fragment_container, paymentMethodFragment)
                .addToBackStack(SelectPaymentMethodFragment().TAG)
                .commit()
    }
}
