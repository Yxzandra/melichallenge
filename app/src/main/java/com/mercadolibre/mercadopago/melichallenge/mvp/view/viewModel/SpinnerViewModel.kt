package com.mercadolibre.mercadopago.melichallenge.mvp.view.viewModel;

import android.arch.lifecycle.ViewModel;

import com.mercadolibre.mercadopago.melichallenge.schemas.ModelSpinner;

class SpinnerViewModel : ViewModel(){
    var modelSpinnerList: List<ModelSpinner>? = null
    var positionSpinnerList : Int = 0

}
