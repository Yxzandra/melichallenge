package com.mercadolibre.mercadopago.melichallenge.mvp.view.viewInterface

interface EnterAmountView {
    fun showError(code:Int, message:String)
    fun showView()
}
