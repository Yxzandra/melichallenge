package com.mercadolibre.mercadopago.melichallenge.mvp.presenter


import com.mercadolibre.mercadopago.melichallenge.mvp.model.SelectPaymentMethodModel
import com.mercadolibre.mercadopago.melichallenge.mvp.model.modelInterface.OnPaymentMethodFinishedListener
import com.mercadolibre.mercadopago.melichallenge.mvp.view.SelectPaymentMethodFragment
import com.mercadolibre.mercadopago.melichallenge.schemas.ModelSpinner
import com.mercadolibre.mercadopago.melichallenge.schemas.response.ErrorResponse
import com.mercadolibre.mercadopago.melichallenge.schemas.response.PaymentMethod

class SelectPaymentMethodPresenter(private var view : SelectPaymentMethodFragment, private var model : SelectPaymentMethodModel) : OnPaymentMethodFinishedListener {

    override fun onError(body : ErrorResponse) {
        view.hideProgress()
        view.showHttpError(body.status, body.message!!)
    }

    override fun onSuccess(response : List<PaymentMethod>) {
        view.hideProgress()
        view.showList(loadSpinnerModel(response))
    }

    fun loadSpinnerModel(paymentMethods : List<PaymentMethod> ) : MutableList<ModelSpinner> {
        val modelSpinnerList : MutableList<ModelSpinner> = arrayListOf()
        for (paymentMethod in paymentMethods) {
            val modelSpinner = ModelSpinner()
            modelSpinner.id = paymentMethod.id
            modelSpinner.name = paymentMethod.name
            modelSpinner.thumbnail = paymentMethod.thumbnail

            modelSpinnerList.add(modelSpinner)
        }
        return modelSpinnerList
    }

    fun getAllPaymentMethod() {
        model.getAllPaymentMethod(this)
        view.showProgress()
    }
}
