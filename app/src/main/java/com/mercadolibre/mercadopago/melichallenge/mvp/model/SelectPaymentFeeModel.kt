package com.mercadolibre.mercadopago.melichallenge.mvp.model

import com.mercadolibre.mercadopago.melichallenge.BuildConfig
import com.mercadolibre.mercadopago.melichallenge.api.ServicesCallback
import com.mercadolibre.mercadopago.melichallenge.api.retrofitClient
import com.mercadolibre.mercadopago.melichallenge.mvp.model.modelInterface.OnPaymentFeeModelFinishedListener
import com.mercadolibre.mercadopago.melichallenge.schemas.response.ErrorResponse
import com.mercadolibre.mercadopago.melichallenge.schemas.response.Installment


class SelectPaymentFeeModel {

    fun getInstallments(listener: OnPaymentFeeModelFinishedListener, amount : String, idPaymentMethod: String, idIssuer: String) {
          retrofitClient.getAllInstallments(BuildConfig.API_KEY,amount,idPaymentMethod,idIssuer).enqueue(object : ServicesCallback<List<Installment>>() {
              override fun onSuccess(response: List<Installment>, code: Int) {
                  listener.onSuccess(response)
              }

              override fun onError(body: ErrorResponse) {
                  listener.onError(body, body.status)
              }

          })
    }
}
