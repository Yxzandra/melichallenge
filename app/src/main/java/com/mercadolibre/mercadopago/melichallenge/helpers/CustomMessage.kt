package com.mercadolibre.mercadopago.melichallenge.helpers;


import android.content.Context
import com.afollestad.materialdialogs.MaterialDialog;
import com.mercadolibre.mercadopago.melichallenge.R


object CustomMessage {

    //Dialogo para el manejo de errores
    @JvmField
    val ERROR : Int = 0

    @JvmField
    val INFORMATION : Int = 1

    @JvmField
    val WARNING : Int = 2

    @JvmField
    val PROBLEMS : Int = 3

    @JvmField
    val WARNING_OK_CANCEL : Int = 4

    @JvmField
    val NO_SUCH_USER : Int = 5

    @JvmField
    val PROGRESSBAR : Int = 6

    @JvmField
    val UNAUTHORIZED : Int = 7

    @JvmField
    val SUCCESS : Int = 8

    @JvmField
    val QUESTION : Int = 9

    /********************************************************************************
     * HTTP STATUS CODES                                                            *
     */
    // Information
    const val CODE_100_INFORMATION = 100 // Continue
    const val CODE_101_INFORMATION = 101 // Switching Protocols
    const val CODE_103_INFORMATION = 103 // Checkpoint

    // Successful
    const val CODE_200_SUCCESSFUL = 200 // OK
    const val CODE_201_SUCCESSFUL = 201 // Created
    const val CODE_202_SUCCESSFUL = 202 // Accepted
    const val CODE_203_SUCCESSFUL = 203 // Non-Authoritative Information
    const val CODE_204_SUCCESSFUL = 204 // No Content
    const val CODE_205_SUCCESSFUL = 205 // Reset Content
    const val CODE_206_SUCCESSFUL = 205 // Partial Content

    // Redirection
    const val CODE_300_REDIRECTION = 300 // Multiple Choices
    const val CODE_301_REDIRECTION = 301 // Moved Permanently
    const val CODE_302_REDIRECTION = 302 // Found
    const val CODE_303_REDIRECTION = 303 // See Other
    const val CODE_304_REDIRECTION = 304 // Not Modified
    const val CODE_306_REDIRECTION = 306 // Switch Proxy
    const val CODE_307_REDIRECTION = 307 // Temporary Redirect
    const val CODE_308_REDIRECTION = 308 // Resume Incomplete

    // Client Error
    const val CODE_400_CLIENT_ERROR = 400 // Bad Request
    const val CODE_401_CLIENT_ERROR = 401 // Unauthorized
    const val CODE_402_CLIENT_ERROR = 402 // Payment Required
    const val CODE_403_CLIENT_ERROR = 403 // Forbidden
    const val CODE_404_CLIENT_ERROR = 404 // Not Found
    const val CODE_405_CLIENT_ERROR = 405 // Method Not Allowed
    const val CODE_406_CLIENT_ERROR = 406 // Not Acceptable
    const val CODE_407_CLIENT_ERROR = 407 // Proxy Authentication Required
    const val CODE_408_CLIENT_ERROR = 408 // Request Timeout
    const val CODE_409_CLIENT_ERROR = 409 // Conflict
    const val CODE_410_CLIENT_ERROR = 410 // Gone
    const val CODE_411_CLIENT_ERROR = 411 // Length Required
    const val CODE_412_CLIENT_ERROR = 412 // Precondition Failed
    const val CODE_413_CLIENT_ERROR = 413 // Request Entity Too Large
    const val CODE_414_CLIENT_ERROR = 414 // Request-URI Too Long
    const val CODE_415_CLIENT_ERROR = 415 // Unsupported Media Type
    const val CODE_416_CLIENT_ERROR = 416 // Requested Range Not Satisfiable
    const val CODE_417_CLIENT_ERROR = 417 // Expectation Failed

    //Server Error
    const val CODE_500_SERVER_ERROR = 500 // Internal Server Error
    const val CODE_501_SERVER_ERROR = 501 // Not Implemented
    const val CODE_502_SERVER_ERROR = 502 // Bad Gateway
    const val CODE_503_SERVER_ERROR = 503 // Service Unavailable
    const val CODE_504_SERVER_ERROR = 504 // Gateway Timeout
    const val CODE_505_SERVER_ERROR = 505 // HTTP Version Not Supported
    const val CODE_511_SERVER_ERROR = 511 // Network Authentication Required


    @JvmStatic
    fun get(context : Context, type: Int, message : String): MaterialDialog.Builder  {
        val dialog : MaterialDialog.Builder

        when (type) {
            ERROR ->
                dialog = MaterialDialog.Builder(context)
                        .title(R.string.error)
                        .iconRes(R.drawable.ic_error)
                        .maxIconSizeRes(R.dimen.dialog_max_icon_size)
                        .content(message)
                        .positiveText(R.string.accept)
            INFORMATION->
                dialog = MaterialDialog.Builder(context)
                        .title(R.string.information)
                        .iconRes(R.drawable.ic_information)
                        .maxIconSizeRes(R.dimen.dialog_max_icon_size)
                        .content(message)
                        .positiveText(R.string.accept)
            SUCCESS->
                dialog = MaterialDialog.Builder(context)
                        .title(R.string.success)
                        .iconRes(R.drawable.ic_success)
                        .maxIconSizeRes(R.dimen.dialog_max_icon_size)
                        .content(message)
                        .positiveText(R.string.accept)
            WARNING->
                dialog = MaterialDialog.Builder(context)
                        .title(R.string.warning)
                        .iconRes(R.drawable.ic_warning)
                        .maxIconSizeRes(R.dimen.dialog_max_icon_size)
                        .content(message)
                        .positiveText(R.string.accept)
            PROBLEMS->
                dialog = MaterialDialog.Builder(context)
                        .title(R.string.problems)
                        .iconRes(R.drawable.ic_problems)
                        .maxIconSizeRes(R.dimen.dialog_max_icon_size)
                        .content(message)
                        .positiveText(R.string.accept)
            WARNING_OK_CANCEL->
                dialog = MaterialDialog.Builder(context)
                        .title(R.string.warning)
                        .iconRes(R.drawable.ic_warning)
                        .maxIconSizeRes(R.dimen.dialog_max_icon_size)
                        .content(message)
                        .positiveText(R.string.accept)
                        .negativeText(R.string.cancel)

            NO_SUCH_USER ->
                dialog = MaterialDialog.Builder(context)
                        .title(R.string.error)
                        .iconRes(R.drawable.ic_no_such_user)
                        .maxIconSizeRes(R.dimen.dialog_max_icon_size)
                        .content(message)
                        .positiveText(R.string.accept)
            PROGRESSBAR->
                dialog = MaterialDialog.Builder(context)
                        .title(R.string.please_wait)
                        .content(message)
                        .cancelable(false)
                        .autoDismiss(false)
                        .progress(true, 0)
            UNAUTHORIZED ->
                dialog = MaterialDialog.Builder(context)
                        .title(R.string.unauthorized)
                        .iconRes(R.drawable.ic_prohibition)
                        .maxIconSizeRes(R.dimen.dialog_max_icon_size)
                        .content(message)
                        .positiveText(R.string.accept)
            QUESTION->
                dialog = MaterialDialog.Builder(context)
                        .title(R.string.question)
                        .iconRes(R.drawable.ic_question)
                        .maxIconSizeRes(R.dimen.dialog_max_icon_size)
                        .content(message)
                        .positiveText(R.string.accept)
                        .negativeText(R.string.cancel)

            else ->
                dialog = MaterialDialog.Builder(context)
                        .title(R.string.question)
                        .iconRes(R.drawable.ic_question)
                        .maxIconSizeRes(R.dimen.dialog_max_icon_size)
                        .content(message)
                        .positiveText(R.string.accept)
                        .negativeText(R.string.cancel)
        }


        return dialog
    }

    @JvmStatic
    fun handlerRequestError(context: Context, code: Int, message: String): MaterialDialog {

        when (code) {
            CODE_401_CLIENT_ERROR -> return CustomMessage.get(context,
                    CustomMessage.UNAUTHORIZED,
                    if (message.isEmpty()) context.resources.getString(R.string.unauthorized_user) else message)
                    .cancelable(false)
                    .show()

            CODE_404_CLIENT_ERROR -> return CustomMessage.get(context,
                    CustomMessage.PROBLEMS,
                    if (message.isEmpty()) context.resources.getString(R.string.service_not_found) else message)
                    .cancelable(false)
                    .show()
            CODE_400_CLIENT_ERROR -> return CustomMessage.get(context,
                    CustomMessage.PROBLEMS,
                    if (message.isEmpty()) context.resources.getString(R.string.msg_error_no_response) else message)
                    .cancelable(false)
                    .show()
            CODE_500_SERVER_ERROR -> return CustomMessage.get(context,
                    CustomMessage.PROBLEMS,
                    if (message.isEmpty()) context.resources.getString(R.string.server_error) else message)
                    .cancelable(false)
                    .show()
            CODE_200_SUCCESSFUL -> return CustomMessage.get(context,
                    CustomMessage.SUCCESS,
                    if (message.isEmpty()) context.resources.getString(R.string.purchase_successful) else message)
                    .cancelable(false)
                    .show()
            CODE_100_INFORMATION -> return CustomMessage.get(context,
                    CustomMessage.INFORMATION,
                    message)
                    .cancelable(false)
                    .show()
            else -> return CustomMessage.get(context,
                    CustomMessage.ERROR,
                    if (message.isEmpty()) context.resources.getString(R.string.msg_error_no_response) else message)
                    .cancelable(false)
                    .show()
        }
    }

}
