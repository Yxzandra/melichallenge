package com.mercadolibre.mercadopago.melichallenge.schemas.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Cause {

    @SerializedName("code")
    @Expose
    var code : String? = null

    @SerializedName("description")
    @Expose
    var description : String? = null

}
