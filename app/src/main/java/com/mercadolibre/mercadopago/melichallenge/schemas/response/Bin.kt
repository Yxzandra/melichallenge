package com.mercadolibre.mercadopago.melichallenge.schemas.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Bin {

    @SerializedName("pattern")
    @Expose
    var pattern : String? = null

    @SerializedName("installments_pattern")
    @Expose
    var installmentsPattern : String? = null

    @SerializedName("exclusion_pattern")
    @Expose
    var exclusionPattern : String? = null
}
