package com.mercadolibre.mercadopago.melichallenge.api

import com.mercadolibre.mercadopago.melichallenge.schemas.response.Card
import com.mercadolibre.mercadopago.melichallenge.schemas.response.Installment
import com.mercadolibre.mercadopago.melichallenge.schemas.response.PaymentMethod
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    //Trae todos los métodos de pago
    @GET("/v1/payment_methods")
    fun getPaymentMethod(@Query("public_key") public_key: String): Call<List<PaymentMethod>>

    //Trae todos los bancos disponibles para ese método de pago
    @GET("/v1/payment_methods/card_issuers")
    fun getAllCardIssuers(@Query("public_key") public_key: String,
                          @Query("payment_method_id") payment_method_id: String): Call<List<Card>>

    //Trae los tipos de cuotas disponibles para el banco seleccionado
    @GET("/v1/payment_methods/installments")
    fun getAllInstallments(@Query("public_key") public_key: String,
                           @Query("amount") amount: String,
                           @Query("payment_method_id") payment_method_id: String,
                           @Query("issuer.id") issuer_id: String): Call<List<Installment>>

}