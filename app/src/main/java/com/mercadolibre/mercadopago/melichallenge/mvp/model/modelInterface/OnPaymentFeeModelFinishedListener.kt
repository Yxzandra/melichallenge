package com.mercadolibre.mercadopago.melichallenge.mvp.model.modelInterface

import com.mercadolibre.mercadopago.melichallenge.schemas.response.ErrorResponse
import com.mercadolibre.mercadopago.melichallenge.schemas.response.Installment

interface OnPaymentFeeModelFinishedListener {
    fun onError(body : ErrorResponse, code: Int)

    fun onSuccess( response: List<Installment>)
}