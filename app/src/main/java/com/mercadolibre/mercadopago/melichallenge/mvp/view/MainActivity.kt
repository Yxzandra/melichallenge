package com.mercadolibre.mercadopago.melichallenge.mvp.view

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity

import com.mercadolibre.mercadopago.melichallenge.R
import com.mercadolibre.mercadopago.melichallenge.mvp.view.viewModel.MainActivityViewModel

class MainActivity : AppCompatActivity( ) {
    val TAG : String = "MainActivity"
    private val viewModel: MainActivityViewModel by lazy {
        ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (viewModel.backStackEntryCount == 0) {
            showView()
        }
    }

    override fun onBackPressed() {
       if (supportFragmentManager.backStackEntryCount == 1)
           finish()
        super.onBackPressed()
    }

    override fun startActivityForResult(intent: Intent, requestCode : Int) {
        if (requestCode == Activity.RESULT_OK) {
            deleteBackStack()
            showView()
        }else {
            super.startActivityForResult(intent, requestCode)
        }
    }



    override fun onDestroy() {
        super.onDestroy()
        viewModel.backStackEntryCount = supportFragmentManager.backStackEntryCount
    }

    private fun deleteBackStack(){
        val fragmentManager : FragmentManager = supportFragmentManager
        if (fragmentManager.backStackEntryCount > 0) {
            for (i in 0 until fragmentManager.backStackEntryCount)
                fragmentManager.popBackStack()
        }
    }

    private fun showView() {
        val fragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction : FragmentTransaction = fragmentManager.beginTransaction()
        val enterAmountFragment = EnterAmountFragment()
        fragmentTransaction.replace(R.id.fragment_container, enterAmountFragment)
                .addToBackStack(EnterAmountFragment().TAG)
                .commit()
    }

}
