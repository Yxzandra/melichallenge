package com.mercadolibre.mercadopago.melichallenge.mvp.view

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.afollestad.materialdialogs.MaterialDialog
import com.mercadolibre.mercadopago.melichallenge.R
import com.mercadolibre.mercadopago.melichallenge.helpers.CustomMessage
import com.mercadolibre.mercadopago.melichallenge.mvp.model.SelectPaymentMethodModel
import com.mercadolibre.mercadopago.melichallenge.mvp.presenter.SelectPaymentMethodPresenter
import com.mercadolibre.mercadopago.melichallenge.mvp.view.viewInterface.BaseView
import com.mercadolibre.mercadopago.melichallenge.mvp.view.viewModel.SpinnerViewModel
import com.mercadolibre.mercadopago.melichallenge.schemas.ModelSpinner
import kotlinx.android.synthetic.main.fragment_payment.*
import java.util.*


class SelectPaymentMethodFragment : Fragment(), BaseView, AdapterView.OnItemSelectedListener, View.OnClickListener{
    val TAG: String = "SelectPaymentMethodFragment"

    private lateinit var mPresenter : SelectPaymentMethodPresenter
    private lateinit var mDialog : MaterialDialog
    private lateinit var idPaymentMethodSelect : String
    private lateinit var amount : String
    private lateinit var namePaymentMethodSelect : String
    private val viewModel: SpinnerViewModel by lazy {
        ViewModelProviders.of(this).get(SpinnerViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_payment, container, false)
        amount = arguments!!.getString(getString(R.string.bundle_amount))

        mDialog = CustomMessage.get(activity!!, CustomMessage.PROGRESSBAR, resources.getString(R.string.please_event)).build()
        this.mPresenter = SelectPaymentMethodPresenter(this, SelectPaymentMethodModel())
        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        layoutFragment.visibility = View.GONE
        button_accept.setOnClickListener(this)
        txt_title.text = getString(R.string.payment_method_title)

        if (viewModel.modelSpinnerList == null){
            this.mPresenter.getAllPaymentMethod()

        }else {
            showList(viewModel.modelSpinnerList!!)
            spinner.setSelection(viewModel.positionSpinnerList)
        }


    }

    override fun showProgress() {
        mDialog.show()
    }

    override fun hideProgress() {
        mDialog.hide()
    }

    override fun showHttpError(code: Int, message: String) {
        CustomMessage.handlerRequestError(Objects.requireNonNull(activity)!!, code, message)
    }

    override fun showList(list: List<ModelSpinner>) {
        val adapter = SpinnerAdapter(activity!!, list)
        spinner.adapter = adapter
        spinner.onItemSelectedListener = this
        layoutFragment.visibility = View.VISIBLE
        viewModel.modelSpinnerList = list
    }

    override fun onStop() {
        super.onStop()
        if (mDialog.isCancelled) {
            mDialog.dismiss()
            mDialog.cancel()
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented")
    }

    override fun onItemSelected(parent: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
        val item = parent!!.getItemAtPosition(position) as ModelSpinner
        idPaymentMethodSelect = item.id!!
        namePaymentMethodSelect = item.name!!
        viewModel.positionSpinnerList = position
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.button_accept -> {
               openFragment()
            }
        }
    }


    fun openFragment() {
        val bundle = Bundle()
        bundle.putString(getString(R.string.bundle_amount), amount)
        bundle.putString(getString(R.string.bundle_paymentmethod), idPaymentMethodSelect)
        bundle.putString(getString(R.string.bundle_namepaymentmethod), namePaymentMethodSelect)

        val selectBankFragment = SelectBankFragment()
        selectBankFragment.arguments = bundle
        activity!!.supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, selectBankFragment)
                .addToBackStack(SelectBankFragment().TAG)
                .commit()
        }


}
