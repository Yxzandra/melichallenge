package com.mercadolibre.mercadopago.melichallenge.mvp.model

import com.mercadolibre.mercadopago.melichallenge.BuildConfig
import com.mercadolibre.mercadopago.melichallenge.api.ServicesCallback
import com.mercadolibre.mercadopago.melichallenge.api.retrofitClient
import com.mercadolibre.mercadopago.melichallenge.mvp.model.modelInterface.OnPaymentMethodFinishedListener
import com.mercadolibre.mercadopago.melichallenge.schemas.response.ErrorResponse
import com.mercadolibre.mercadopago.melichallenge.schemas.response.PaymentMethod

class SelectPaymentMethodModel {

    fun getAllPaymentMethod(listener : OnPaymentMethodFinishedListener) {
        retrofitClient.getPaymentMethod(BuildConfig.API_KEY).enqueue(object : ServicesCallback<List<PaymentMethod>>() {

            override fun onSuccess(response: List<PaymentMethod>, code: Int) {
                listener.onSuccess(response)
            }

            override fun onError(body: ErrorResponse) {
                listener.onError(body)
            }

         })


    }
}
