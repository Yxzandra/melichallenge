package com.mercadolibre.mercadopago.melichallenge.mvp.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.mercadolibre.mercadopago.melichallenge.R
import com.mercadolibre.mercadopago.melichallenge.helpers.Utils
import com.mercadolibre.mercadopago.melichallenge.schemas.ModelSpinner


class SpinnerAdapter(private val mContext: Context, private var contentArray: List<ModelSpinner>) : BaseAdapter() {

    private val mInflater: LayoutInflater = LayoutInflater.from(mContext)

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        return getCustomView(position, parent!!)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        return getCustomView(position, parent!!)
    }

    fun getCustomView(position : Int, parent: ViewGroup) : View {
        val item = contentArray[position]

        val row: View? = mInflater.inflate(R.layout.item_spinner, parent, false)

        val textView : TextView = row!!.findViewById(R.id.txt_payment)
        textView.text = item.name

        val imageView: ImageView = row.findViewById(R.id.img_payment)
        if (item.thumbnail != null){
            Glide.with(mContext)
                    .load(contentArray[position].thumbnail)
                    .apply(Utils.glideRequestOpcion())
                    .into(imageView)
        }else
            imageView.visibility = View.GONE

        return row
    }
    override fun getItem(position: Int): Any {
        return contentArray[position]
    }

    override fun getItemId(position: Int): Long {
        return contentArray.indexOf(getItem(position)).toLong()
    }

    override fun getCount(): Int {
        return contentArray.size
    }
}


