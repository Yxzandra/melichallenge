package com.mercadolibre.mercadopago.melichallenge.schemas.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Card {

    @SerializedName("id")
    @Expose
    var id : String? = null

    @SerializedName("name")
    @Expose
    var name : String? = null

    @SerializedName("secure_thumbnail")
    @Expose
    var secureThumbnail : String? = null

    @SerializedName("thumbnail")
    @Expose
    var thumbnail : String? = null

    @SerializedName("processing_mode")
    @Expose
    var processingMode : String? = null

}