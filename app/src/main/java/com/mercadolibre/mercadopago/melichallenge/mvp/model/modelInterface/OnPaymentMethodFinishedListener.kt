package com.mercadolibre.mercadopago.melichallenge.mvp.model.modelInterface

import com.mercadolibre.mercadopago.melichallenge.schemas.response.ErrorResponse
import com.mercadolibre.mercadopago.melichallenge.schemas.response.PaymentMethod

interface OnPaymentMethodFinishedListener {
    fun onError(body : ErrorResponse)

    fun onSuccess(response: List<PaymentMethod> )
}
