package com.mercadolibre.mercadopago.melichallenge.mvp.view

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.afollestad.materialdialogs.MaterialDialog
import com.mercadolibre.mercadopago.melichallenge.R
import com.mercadolibre.mercadopago.melichallenge.helpers.CustomMessage
import com.mercadolibre.mercadopago.melichallenge.mvp.model.SelectBankModel
import com.mercadolibre.mercadopago.melichallenge.mvp.presenter.SelectBankPresenter
import com.mercadolibre.mercadopago.melichallenge.mvp.view.viewInterface.SelectBankView
import com.mercadolibre.mercadopago.melichallenge.mvp.view.viewModel.SpinnerViewModel
import com.mercadolibre.mercadopago.melichallenge.schemas.ModelSpinner
import kotlinx.android.synthetic.main.fragment_payment.*


class SelectBankFragment: Fragment(), SelectBankView, AdapterView.OnItemSelectedListener, View.OnClickListener {
    val TAG : String = "SelectBankFragment"

    private lateinit var mPresenter : SelectBankPresenter
    private lateinit var mDialog : MaterialDialog
    private lateinit var bankSelect : String
    private lateinit var amount : String
    private lateinit var idPaymentMethod : String
    private lateinit var nameBankSelect : String
    private lateinit var namePaymentMethod : String
    private val viewModel: SpinnerViewModel by lazy {
        ViewModelProviders.of(this).get(SpinnerViewModel::class.java)
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view : View = inflater.inflate(R.layout.fragment_payment, container, false)

        amount = arguments!!.getString(getString(R.string.bundle_amount))
        idPaymentMethod = arguments!!.getString(getString(R.string.bundle_paymentmethod))
        namePaymentMethod = arguments!!.getString(getString(R.string.bundle_namepaymentmethod))
        
        mDialog = CustomMessage.get(activity!!, CustomMessage.PROGRESSBAR, resources.getString(R.string.please_event)).build()
        this.mPresenter = SelectBankPresenter(this, SelectBankModel())

        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        layoutFragment.visibility = View.GONE
        button_accept.setOnClickListener(this)
        txt_title.text = getString(R.string.bank_title)

        if (viewModel.modelSpinnerList == null){
            this.mPresenter.getAllBank(idPaymentMethod)
        }else {
            showList(viewModel.modelSpinnerList!!)
            spinner.setSelection(viewModel.positionSpinnerList)
        }
    }

    override fun showProgress() {
        mDialog.show()
    }

    override fun hideProgress() {
        mDialog.hide()
    }

    override fun showHttpError(code: Int, message: String) { 
        CustomMessage.handlerRequestError(activity!!, code, message)
    }

    override fun showList(list: List<ModelSpinner>) {
        val adapter = SpinnerAdapter(activity!!, list)
        spinner.adapter = adapter
        spinner.onItemSelectedListener = this
        layoutFragment.visibility = View.VISIBLE
        viewModel.modelSpinnerList = list
    }

    override fun showInformation() { 
        CustomMessage.handlerRequestError(activity!!,
                CustomMessage.CODE_100_INFORMATION,
                getString(R.string.error_list_card))
                .setOnDismissListener {
            activity!!.onBackPressed()
        }
    }

    override fun onStop() {
        super.onStop()
        if (mDialog.isCancelled) {
            mDialog.dismiss()
            mDialog.cancel()
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
        val item = parent!!.getItemAtPosition(position) as ModelSpinner

        bankSelect = item.id!!
        nameBankSelect = item.name!!
        viewModel.positionSpinnerList = position
    }


    override fun onClick(p0: View?) {
        val bundle = Bundle()
        bundle.putString(getString(R.string.bundle_amount), amount)
        bundle.putString(getString(R.string.bundle_paymentmethod), idPaymentMethod)
        bundle.putString(getString(R.string.bundle_namepaymentmethod), namePaymentMethod)
        bundle.putString(getString(R.string.bundle_bank), bankSelect)
        bundle.putString(getString(R.string.bundle_namebank), nameBankSelect)

        val selectPaymentFeeFragment = SelectPaymentFeeFragment()
        selectPaymentFeeFragment.arguments = bundle
        activity!!.supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, selectPaymentFeeFragment)
                .addToBackStack(SelectPaymentFeeFragment().TAG)
                .commit()
    }

}
