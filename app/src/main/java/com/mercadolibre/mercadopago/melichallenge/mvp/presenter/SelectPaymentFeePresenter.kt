package com.mercadolibre.mercadopago.melichallenge.mvp.presenter

import com.mercadolibre.mercadopago.melichallenge.mvp.model.SelectPaymentFeeModel
import com.mercadolibre.mercadopago.melichallenge.mvp.model.modelInterface.OnPaymentFeeModelFinishedListener
import com.mercadolibre.mercadopago.melichallenge.mvp.view.SelectPaymentFeeFragment
import com.mercadolibre.mercadopago.melichallenge.schemas.ModelSpinner
import com.mercadolibre.mercadopago.melichallenge.schemas.response.ErrorResponse
import com.mercadolibre.mercadopago.melichallenge.schemas.response.Installment

class SelectPaymentFeePresenter (private var view: SelectPaymentFeeFragment, private var model: SelectPaymentFeeModel): OnPaymentFeeModelFinishedListener {

    override fun onError(body: ErrorResponse, code: Int) {
        view.hideProgress()
        view.showHttpError(code, body.message!!)
    }

    override fun onSuccess(response: List<Installment>) {
        view.hideProgress()
        if (response.isNotEmpty())
            view.showList(loadSpinnerModel(response[0]))
        else
            view.showInformation()

    }

    fun loadSpinnerModel(installment :Installment ) : MutableList<ModelSpinner> {
        val  modelSpinnerList : MutableList<ModelSpinner> = arrayListOf()

        for (payerCost in installment.payerCosts!!) {
            val modelSpinner = ModelSpinner()
            modelSpinner.id = payerCost.installments.toString()
            modelSpinner.name = payerCost.recommendedMessage
            modelSpinnerList.add(modelSpinner)
        }
        return modelSpinnerList
    }

    fun getInstallments(amount : String , idPaymentMethod : String , idIssuer : String ) {
        model.getInstallments(this, amount, idPaymentMethod, idIssuer)
        view.showProgress()
    }
}
