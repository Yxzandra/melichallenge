package com.mercadolibre.mercadopago.melichallenge.schemas.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CardNumber {

    @SerializedName("tournvalidationament")
    @Expose
    var validation : String? = null


    @SerializedName("length")
    @Expose
    var length : Int? = null
}
