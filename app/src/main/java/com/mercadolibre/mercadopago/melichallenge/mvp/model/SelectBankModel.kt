package com.mercadolibre.mercadopago.melichallenge.mvp.model

import com.mercadolibre.mercadopago.melichallenge.BuildConfig
import com.mercadolibre.mercadopago.melichallenge.api.ServicesCallback
import com.mercadolibre.mercadopago.melichallenge.api.retrofitClient
import com.mercadolibre.mercadopago.melichallenge.mvp.model.modelInterface.OnBankModelFinishedListener
import com.mercadolibre.mercadopago.melichallenge.schemas.response.Card
import com.mercadolibre.mercadopago.melichallenge.schemas.response.ErrorResponse

class SelectBankModel {

    fun getAllBank(listener: OnBankModelFinishedListener, idPaymentMethod: String) {
        retrofitClient.getAllCardIssuers(BuildConfig.API_KEY,idPaymentMethod).enqueue(object : ServicesCallback<List<Card>>() {
            override fun onSuccess(response: List<Card>, code: Int) {
                listener.onSuccess(response )
            }

            override fun onError(body: ErrorResponse) {
                listener.onError(body, body.status)
            }

        })


    }
}