package com.mercadolibre.mercadopago.melichallenge.mvp.view.viewInterface

import com.mercadolibre.mercadopago.melichallenge.schemas.ModelSpinner

interface BaseView{
    fun showProgress()
    fun hideProgress()
    fun showHttpError(code:Int, message: String)
    fun showList(list : List<ModelSpinner> )

}