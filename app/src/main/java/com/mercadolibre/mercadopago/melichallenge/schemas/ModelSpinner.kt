package com.mercadolibre.mercadopago.melichallenge.schemas

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ModelSpinner {

    @SerializedName("id")
    @Expose
    var id : String? = null

    @SerializedName("name")
    @Expose
    var name : String? = null

    @SerializedName("secureThumbnail")
    @Expose
    var secureThumbnail : String? = null

    @SerializedName("thumbnail")
    @Expose
    var thumbnail : String? = null

}
