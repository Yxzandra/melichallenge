package com.mercadolibre.mercadopago.melichallenge.mvp.view

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.afollestad.materialdialogs.MaterialDialog
import com.mercadolibre.mercadopago.melichallenge.R
import com.mercadolibre.mercadopago.melichallenge.helpers.CustomMessage
import com.mercadolibre.mercadopago.melichallenge.mvp.model.SelectPaymentFeeModel
import com.mercadolibre.mercadopago.melichallenge.mvp.presenter.SelectPaymentFeePresenter
import com.mercadolibre.mercadopago.melichallenge.mvp.view.viewInterface.SelectPaymentFeeView
import com.mercadolibre.mercadopago.melichallenge.mvp.view.viewModel.SpinnerViewModel
import com.mercadolibre.mercadopago.melichallenge.schemas.ModelSpinner
import kotlinx.android.synthetic.main.fragment_payment.*


class SelectPaymentFeeFragment : Fragment(), SelectPaymentFeeView, AdapterView.OnItemSelectedListener, View.OnClickListener {
    val TAG = "SelectPaymentFeeFragment"

    private lateinit var mPresenter : SelectPaymentFeePresenter
    private lateinit var mDialog : MaterialDialog
    private lateinit var installmentSelect : String
    private lateinit var amount : String
    private lateinit var idPaymentMethod : String
    private lateinit var idIssuer : String
    private lateinit var nameIssuer : String
    private lateinit var namePaymentMethod : String
    private val viewModel: SpinnerViewModel by lazy {
        ViewModelProviders.of(this).get(SpinnerViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_payment, container, false)

        amount = arguments!!.getString(getString(R.string.bundle_amount))
        idPaymentMethod = arguments!!.getString(getString(R.string.bundle_paymentmethod))
        namePaymentMethod = arguments!!.getString(getString(R.string.bundle_namepaymentmethod))
        idIssuer = arguments!!.getString(getString(R.string.bundle_bank))
        nameIssuer = arguments!!.getString(getString(R.string.bundle_namebank))

        mDialog = CustomMessage.get(activity!!, CustomMessage.PROGRESSBAR, resources.getString(R.string.please_event)).build()
        this.mPresenter = SelectPaymentFeePresenter(this, SelectPaymentFeeModel())


        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txt_title.text = getString(R.string.installments_title)
        button_accept.text = getString(R.string.pay)
        button_accept.setOnClickListener(this)
        layoutFragment.visibility = View.GONE

        if (viewModel.modelSpinnerList == null){
            this.mPresenter.getInstallments(amount, idPaymentMethod, idIssuer)
        }else {
            showList(viewModel.modelSpinnerList!!)
            spinner.setSelection(viewModel.positionSpinnerList)
        }


    }

    override fun showProgress() {
        mDialog.show()
    }

    override fun hideProgress() {
        mDialog.hide()
    }

    override fun showHttpError(code: Int, message: String) {
         CustomMessage.handlerRequestError(activity!!, code, message)
    }

    override fun showList(list: List<ModelSpinner>) {
        val adapter = SpinnerAdapter(activity!!, list)
        spinner.adapter = adapter
        spinner.onItemSelectedListener = this
        layoutFragment.visibility = View.VISIBLE
    }

    override fun showInformation() {
        CustomMessage.handlerRequestError(activity!!,
                CustomMessage.CODE_100_INFORMATION,
                getString(R.string.error_list_card))
                .setOnDismissListener {
                    activity!!.onBackPressed()
                }
    }

    override fun onStop() {
        super.onStop()
        if (mDialog.isCancelled) {
            mDialog.dismiss()
            mDialog.cancel()
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
        val item = parent!!.getItemAtPosition(position) as ModelSpinner

        installmentSelect = item.name!!
         viewModel.positionSpinnerList = position
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.button_accept -> {
                openResult()
            }
        }
    }

    private fun openResult() {
        CustomMessage.handlerRequestError(activity!!, CustomMessage.CODE_200_SUCCESSFUL,
              String.format(activity!!.getString(R.string.purchase_successful), nameIssuer, namePaymentMethod, installmentSelect))
                  .setOnDismissListener {
                      //Se envia a la actividad para recargue el primer fragmento y empiece el ciclo de nuevo

                      val intent = Intent(activity, MainActivity::class.java)
                      activity!!.startActivityForResult(intent, Activity.RESULT_OK)
                     // startActivityForResult(intent, Activity.RESULT_OK)
                  }

    }

}
