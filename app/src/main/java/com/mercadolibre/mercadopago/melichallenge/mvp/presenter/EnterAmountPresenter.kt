package com.mercadolibre.mercadopago.melichallenge.mvp.presenter;

import android.content.Context
import com.mercadolibre.mercadopago.melichallenge.R
import com.mercadolibre.mercadopago.melichallenge.helpers.CustomMessage
import com.mercadolibre.mercadopago.melichallenge.mvp.view.viewInterface.EnterAmountView


class EnterAmountPresenter(private var view : EnterAmountView) {
    val maxAllowedAmount = 250000
    val minAllowedAmount = 0

    fun validateAmount(amount : String, context: Context) {
        if (amount.isEmpty())
            view.showError(CustomMessage.CODE_100_INFORMATION,context.getString(R.string.amount_empty))
        else {
            val amountValidate: Double = amount.toDouble()
            if (amountValidate < maxAllowedAmount && amountValidate > minAllowedAmount) {
                view.showView()
            } else {
                //var result = String.format(context.getString(R.string.amount_error), minAllowedAmount, maxAllowedAmount)
                view.showError(CustomMessage.CODE_100_INFORMATION, context.getString(R.string.amount_error))
            }
        }

    }

}
