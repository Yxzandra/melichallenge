package com.mercadolibre.mercadopago.melichallenge.mvp.presenter

import com.mercadolibre.mercadopago.melichallenge.mvp.model.SelectBankModel
import com.mercadolibre.mercadopago.melichallenge.mvp.model.modelInterface.OnBankModelFinishedListener
import com.mercadolibre.mercadopago.melichallenge.mvp.view.viewInterface.SelectBankView
import com.mercadolibre.mercadopago.melichallenge.schemas.ModelSpinner
import com.mercadolibre.mercadopago.melichallenge.schemas.response.Card
import com.mercadolibre.mercadopago.melichallenge.schemas.response.ErrorResponse

class SelectBankPresenter (private var view : SelectBankView, private var model : SelectBankModel) : OnBankModelFinishedListener {

    override fun onError(body: ErrorResponse, code: Int) {
        view.hideProgress()
        view.showHttpError(code, body.message!!)
    }

    override fun onSuccess(response: List<Card>) {
        view.hideProgress()
        if (response.isNotEmpty())
            view.showList(loadSpinnerModel(response))
        else
            view.showInformation()
    }

    fun loadSpinnerModel(banks : List<Card> ) : MutableList<ModelSpinner> {
        val modelSpinnerList : MutableList<ModelSpinner> = arrayListOf()

        for (bank in banks) {
            val modelSpinner = ModelSpinner()
            modelSpinner.id = bank.id
            modelSpinner.name = bank.name
            modelSpinner.thumbnail = bank.thumbnail
            modelSpinnerList.add(modelSpinner)
        }
        return modelSpinnerList
    }

    fun getAllBank(idPaymentMethod : String ) {
        model.getAllBank(this,idPaymentMethod)
        view.showProgress()
    }
}
