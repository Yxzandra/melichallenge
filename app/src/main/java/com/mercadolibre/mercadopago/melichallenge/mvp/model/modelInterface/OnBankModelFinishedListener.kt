package com.mercadolibre.mercadopago.melichallenge.mvp.model.modelInterface

import com.mercadolibre.mercadopago.melichallenge.schemas.response.Card
import com.mercadolibre.mercadopago.melichallenge.schemas.response.ErrorResponse

interface OnBankModelFinishedListener {
    fun onError(body: ErrorResponse, code:Int)

    fun onSuccess( response: List<Card>)
}
