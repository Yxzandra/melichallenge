package com.mercadolibre.mercadopago.melichallenge.helpers;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions

import com.mercadolibre.mercadopago.melichallenge.R;

object Utils {

    /********************************************************************************
     *  REQUESTOPTION DE GLIDE GENÉRICO                                            *
     ********************************************************************************/
    @JvmStatic
    fun glideRequestOpcion(): RequestOptions {
        return RequestOptions()
                .fitCenter()
                .placeholder(R.drawable.ic_credit_card)
                .error(R.drawable.ic_error)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
    }
}
