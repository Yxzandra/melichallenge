package com.mercadolibre.mercadopago.melichallenge.schemas.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PayerCost {

    @SerializedName("installments")
    @Expose
    var installments : Int? = null

    @SerializedName("installment_rate")
    @Expose
    var installmentRate : Double? = null

    @SerializedName("discount_rate")
    @Expose
    var discountRate : Int? = null

    @SerializedName("labels")
    @Expose
    var labels : List<String>? = null

    @SerializedName("installment_rate_collector")
    @Expose
    var installmentRateCollector : List<String>? = null

    @SerializedName("min_allowed_amount")
    @Expose
    var minAllowedAmount : Int? = null

    @SerializedName("max_allowed_amount")
    @Expose
    var maxAllowedAmount : Int? = null

    @SerializedName("recommended_message")
    @Expose
    var recommendedMessage : String? = null

    @SerializedName("installment_amount")
    @Expose
    var installmentAmount : Double? = null

    @SerializedName("total_amount")
    @Expose
    var totalAmount : Double? = null
}
