package com.mercadolibre.mercadopago.melichallenge;

import android.content.pm.ActivityInfo;
import android.support.test.rule.ActivityTestRule;

import com.mercadolibre.mercadopago.melichallenge.mvp.view.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> rule =
            new ActivityTestRule<>(MainActivity.class, false, true);

    @Test
    public void shouldShowFragmentIfBackStackIsNotEmptyElseCheckActivityIsFinishing() {
        if (rule.getActivity().getSupportFragmentManager().getBackStackEntryCount() != 0){
            onView(withId(R.id.fragment_container)).check(matches(isDisplayed()));
        }else{
            assert(rule.getActivity().isFinishing());
        }
    }

    @Test
    public void shouldKeepFragmentOnScreenRotate() {
        if (rule.getActivity().getSupportFragmentManager().getBackStackEntryCount() != 0){
            onView(withId(R.id.fragment_container)).check(matches(isDisplayed()));
            rule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            onView(withId(R.id.fragment_container)).check(matches(isDisplayed()));

        }
    }


}
