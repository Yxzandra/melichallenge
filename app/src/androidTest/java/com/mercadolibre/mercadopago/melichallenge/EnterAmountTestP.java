package com.mercadolibre.mercadopago.melichallenge;

import android.support.test.rule.ActivityTestRule;

import com.mercadolibre.mercadopago.melichallenge.mvp.view.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.StringEndsWith.endsWith;

public class EnterAmountTestP {

    @Rule
    public ActivityTestRule<MainActivity> rule =
            new ActivityTestRule<>(MainActivity.class, false, true);
    @Test
    public void shouldShowDialogIfAmountIsNotInValidInterval(){
        onView(withId(R.id.edit_amount)).perform(typeText("0"));
        onView(withId(R.id.button_accept)).perform(click());
        onView(isRoot()).inRoot(isDialog()).check(matches(isDisplayed()));
    }

    @Test
    public void showEmptyAmountText(){
        onView(withId(R.id.edit_amount)).perform(typeText(""));
        onView(withId(R.id.button_accept)).perform(click());
        onView(withText(R.string.amount_empty)).inRoot(isDialog()).check(matches(isDisplayed()));
    }

    @Test
    public void shouldShowDialogOnEmptyAmount(){
        onView(withId(R.id.edit_amount)).perform(typeText("250000"));
        onView(withId(R.id.button_accept)).perform(click());
        onView(isRoot()).inRoot(isDialog()).check(matches(isDisplayed()));
    }

    @Test
    public void shouldChangeFragmentAfterChoosingItem() {
        onView(withId(R.id.edit_amount)).perform(typeText("25000"));
        onView(withId(R.id.button_accept)).perform(click());
        onView(withId(R.id.txt_title)).check(matches(withText(endsWith("Seleccione su medio de pago"))));

    }


}

