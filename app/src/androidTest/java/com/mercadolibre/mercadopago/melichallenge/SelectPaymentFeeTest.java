package com.mercadolibre.mercadopago.melichallenge;

import android.content.pm.ActivityInfo;
import android.support.test.rule.ActivityTestRule;

import com.mercadolibre.mercadopago.melichallenge.mvp.view.MainActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public class SelectPaymentFeeTest {
    @Rule
    public ActivityTestRule<MainActivity> rule =
            new ActivityTestRule<>(MainActivity.class, false, true);

    @Before
    public void setup(){
        onView(withId(R.id.edit_amount)).perform(typeText("100"));
        onView(withId(R.id.button_accept)).perform(click());
        onView(withId(R.id.spinner)).perform(click());
        onView(withText("Mastercard")).perform(click());
        onView(withId(R.id.button_accept)).perform(click());
        onView(withId(R.id.spinner)).perform(click());
        onView(withText("Cencosud")).perform(click());
        onView(withId(R.id.button_accept)).perform(click());
    }

    @Test
    public void shouldKeepSpinnerTextOnScreenRotate(){
        String selectionText = "1 cuota de $ 100,00 ($ 100,00)";

        onView(withId(R.id.spinner)).perform(click());
        onView(withText(selectionText)).perform(click());

        rule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        onView(withId(R.id.txt_payment)).check(matches(withText(selectionText)));
    }

    @Test
    public void shouldShowSuccessDialogOnPaymentSuccess(){
        shouldKeepSpinnerTextOnScreenRotate();
        onView(withId(R.id.button_accept)).perform(click());
        onView(isRoot()).inRoot(isDialog()).check(matches(isDisplayed()));

    }


}
