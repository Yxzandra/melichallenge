package com.mercadolibre.mercadopago.melichallenge.presenter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercadolibre.mercadopago.melichallenge.helpers.CustomMessage;
import com.mercadolibre.mercadopago.melichallenge.mvp.view.SelectPaymentFeeFragment;
import com.mercadolibre.mercadopago.melichallenge.mvp.model.SelectPaymentFeeModel;
import com.mercadolibre.mercadopago.melichallenge.mvp.presenter.SelectPaymentFeePresenter;
import com.mercadolibre.mercadopago.melichallenge.schemas.response.ErrorResponse;
import com.mercadolibre.mercadopago.melichallenge.schemas.response.Installment;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class SelectPaymentFeePresenterTest {
    private SelectPaymentFeeFragment view;
    private SelectPaymentFeePresenter presenter;


    @Before
    public void setUp() {
        view = mock(SelectPaymentFeeFragment.class);
    }

    @Test
    public void showldShowInformationMessageOnEmptyInstallmentList(){
        presenter = new SelectPaymentFeePresenter(view, new SelectPaymentFeeModel());
        List<Installment> installmentList = new ArrayList<>();

        presenter.onSuccess(installmentList);
        verify(view, times(1)).showInformation();
    }

    @Test
    public void shouldNotReturnNullModelSpinnerList() throws IOException {
        presenter = new SelectPaymentFeePresenter(view, new SelectPaymentFeeModel());
        Installment installment = mockListCard();
        assertNotNull(presenter.loadSpinnerModel(installment));
    }

    @Test
    public void shouldShowErrorMessageInInstallmentOnErrorStatusCode() throws IOException {
        presenter = new SelectPaymentFeePresenter(view, new SelectPaymentFeeModel());
        ErrorResponse errorResponse = getErrorResponse();
        presenter.onError(errorResponse, CustomMessage.CODE_404_CLIENT_ERROR);
        verify(view, times(1)).showHttpError(CustomMessage.CODE_404_CLIENT_ERROR,errorResponse.getMessage());
        verify(view, times(1)).hideProgress();
    }


    private Installment mockListCard()  throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String itemCard= "{\n" +
                "    \"paymentMethodId\": \"visa\",\n" +
                "    \"paymentTypeId\": \"credit_card\",\n" +
                "    \"issuer\": {},\n" +
                "    \"processingMode\": \"aggregator\",\n" +
                "    \"payerCosts\": [\n" +
                "      {\n" +
                "        \"installments\": 1,\n" +
                "        \"installmentRate\": 0,\n" +
                "        \"discountRate\": 0,\n" +
                "        \"labels\": [\n" +
                "          \"CFT_0,00%|TEA_0,00%\"\n" +
                "        ],\n" +
                "        \"installmentRateCollector\": [\n" +
                "          \"MERCADOPAGO\"\n" +
                "        ],\n" +
                "        \"minAllowedAmount\": 0,\n" +
                "        \"maxAllowedAmount\": 250000,\n" +
                "        \"recommendedMessage\": \"1 cuota de $ 100,00 ($ 100,00)\",\n" +
                "        \"installmentAmount\": 100,\n" +
                "        \"totalAmount\": 100\n" +
                "      },\n" +
                "      {\n" +
                "        \"installments\": 3,\n" +
                "        \"installmentRate\": 19.72,\n" +
                "        \"discountRate\": 0,\n" +
                "        \"labels\": [\n" +
                "          \"CFT_199,44%|TEA_150,35%\"\n" +
                "        ],\n" +
                "        \"installmentRateCollector\": [\n" +
                "          \"MERCADOPAGO\"\n" +
                "        ],\n" +
                "        \"minAllowedAmount\": 2,\n" +
                "        \"maxAllowedAmount\": 250000,\n" +
                "        \"recommendedMessage\": \"3 cuotas de $ 39,91 ($ 119,72)\",\n" +
                "        \"installmentAmount\": 39.91,\n" +
                "        \"totalAmount\": 119.72\n" +
                "      },\n" +
                "      {\n" +
                "        \"installments\": 6,\n" +
                "        \"installmentRate\": 34.49,\n" +
                "        \"discountRate\": 0,\n" +
                "        \"labels\": [\n" +
                "          \"CFT_187,01%|TEA_142,79%\",\n" +
                "          \"recommended_interest_installment_with_some_banks\"\n" +
                "        ],\n" +
                "        \"installmentRateCollector\": [\n" +
                "          \"MERCADOPAGO\"\n" +
                "        ],\n" +
                "        \"minAllowedAmount\": 3,\n" +
                "        \"maxAllowedAmount\": 250000,\n" +
                "        \"recommendedMessage\": \"6 cuotas de $ 22,42 ($ 134,49)\",\n" +
                "        \"installmentAmount\": 22.42,\n" +
                "        \"totalAmount\": 134.49\n" +
                "      },\n" +
                "      {\n" +
                "        \"installments\": 9,\n" +
                "        \"installmentRate\": 49.19,\n" +
                "        \"discountRate\": 0,\n" +
                "        \"labels\": [\n" +
                "          \"CFT_176,57%|TEA_136,21%\"\n" +
                "        ],\n" +
                "        \"installmentRateCollector\": [\n" +
                "          \"MERCADOPAGO\"\n" +
                "        ],\n" +
                "        \"minAllowedAmount\": 5,\n" +
                "        \"maxAllowedAmount\": 250000,\n" +
                "        \"recommendedMessage\": \"9 cuotas de $ 16,58 ($ 149,19)\",\n" +
                "        \"installmentAmount\": 16.58,\n" +
                "        \"totalAmount\": 149.19\n" +
                "      },\n" +
                "      {\n" +
                "        \"installments\": 12,\n" +
                "        \"installmentRate\": 63.77,\n" +
                "        \"discountRate\": 0,\n" +
                "        \"labels\": [\n" +
                "          \"recommendedInstallment\",\n" +
                "          \"CFT_167,52%|TEA_130,33%\"\n" +
                "        ],\n" +
                "        \"installmentRateCollector\": [\n" +
                "          \"MERCADOPAGO\"\n" +
                "        ],\n" +
                "        \"minAllowedAmount\": 6,\n" +
                "        \"maxAllowedAmount\": 250000,\n" +
                "        \"recommendedMessage\": \"12 cuotas de $ 13,65 ($ 163,77)\",\n" +
                "        \"installmentAmount\": 13.65,\n" +
                "        \"totalAmount\": 163.77\n" +
                "      }\n" +
                "    ]\n" +
                "  }";
        return mapper.readValue(itemCard, Installment.class);
    }

    private ErrorResponse getErrorResponse()  throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String itemError= "{\n" +
                "  \"message\": \"invalid parameters\",\n" +
                "  \"error\": \"bad_request\",\n" +
                "  \"status\": 400,\n" +
                "  \"cause\": [\n" +
                "    {\n" +
                "      \"code\": \"2002\",\n" +
                "      \"description\": \"payment method not found\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        return mapper.readValue(itemError, ErrorResponse.class);
    }


}
