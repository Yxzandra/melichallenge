package com.mercadolibre.mercadopago.melichallenge.presenter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercadolibre.mercadopago.melichallenge.helpers.CustomMessage;
import com.mercadolibre.mercadopago.melichallenge.mvp.view.SelectBankFragment;
import com.mercadolibre.mercadopago.melichallenge.mvp.model.SelectBankModel;
import com.mercadolibre.mercadopago.melichallenge.mvp.presenter.SelectBankPresenter;
import com.mercadolibre.mercadopago.melichallenge.schemas.response.Card;
import com.mercadolibre.mercadopago.melichallenge.schemas.response.ErrorResponse;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class SelectBankPresenterTest {
    private SelectBankFragment view;
    private SelectBankPresenter presenter;


    @Before
    public void setUp() {
        view = mock(SelectBankFragment.class);
    }

    @Test
    public void showldShowInformationMessageOnEmptyIssuersList(){
        presenter = new SelectBankPresenter(view, new SelectBankModel());
        List<Card> cardList = new ArrayList<>();

        presenter.onSuccess(cardList);
        verify(view, times(1)).showInformation();
    }

    @Test
    public void shouldNotReturnNullModelSpinnerList() throws IOException {
        presenter = new SelectBankPresenter(view, new SelectBankModel());
        List<Card> cardList = mockListCard();
        assertNotNull(presenter.loadSpinnerModel(cardList));
    }

    @Test
    public void shouldShowErrorMessageInIssuerOnErrorStatusCode() throws IOException {
        presenter = new SelectBankPresenter(view, new SelectBankModel());
        ErrorResponse errorResponse = getErrorResponse();
        presenter.onError(errorResponse, CustomMessage.CODE_400_CLIENT_ERROR);
        verify(view, times(1)).showHttpError(CustomMessage.CODE_400_CLIENT_ERROR,errorResponse.getMessage());
        verify(view, times(1)).hideProgress();
    }


    private List<Card> mockListCard()  throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String itemCard= "{\n" +
                "    \"id\": \"1078\",\n" +
                "    \"name\": \"Mercado Pago + Banco Patagonia\",\n" +
                "    \"secureThumbnail\": \"https://www.mercadopago.com/org-img/MP3/API/logos/1078.gif\",\n" +
                "    \"thumbnail\": \"http://img.mlstatic.com/org-img/MP3/API/logos/1078.gif\",\n" +
                "    \"processingMode\": \"aggregator\"\n" +
                "  }";
        Card card = mapper.readValue(itemCard, Card.class);
        List<Card> cardList = new ArrayList<>();
        cardList.add(card);

        return cardList;
    }

    private ErrorResponse getErrorResponse()  throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String itemError= "{\n" +
                "  \"message\": \"invalid parameters\",\n" +
                "  \"error\": \"bad_request\",\n" +
                "  \"status\": 400,\n" +
                "  \"cause\": [\n" +
                "    {\n" +
                "      \"code\": \"2002\",\n" +
                "      \"description\": \"payment method not found\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        return mapper.readValue(itemError, ErrorResponse.class);
    }


}
