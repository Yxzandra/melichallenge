package com.mercadolibre.mercadopago.melichallenge.presenter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercadolibre.mercadopago.melichallenge.mvp.view.SelectPaymentMethodFragment;
import com.mercadolibre.mercadopago.melichallenge.mvp.model.SelectPaymentMethodModel;
import com.mercadolibre.mercadopago.melichallenge.mvp.presenter.SelectPaymentMethodPresenter;
import com.mercadolibre.mercadopago.melichallenge.schemas.response.ErrorResponse;
import com.mercadolibre.mercadopago.melichallenge.schemas.response.PaymentMethod;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class SelectPaymentMethodPresenterTest {
    private SelectPaymentMethodFragment view;
    private SelectPaymentMethodPresenter presenter;


    @Before
    public void setUp() {
        view = mock(SelectPaymentMethodFragment.class);
    }

    @Test
    public void shouldShowErrorMessageInPaymentMethodOnErrorStatusCode() throws IOException {
        presenter = new SelectPaymentMethodPresenter(view, new SelectPaymentMethodModel());
        ErrorResponse errorResponse = getErrorResponse();
        presenter.onError(errorResponse);
        verify(view, times(1)).showHttpError(400,errorResponse.getMessage());
        verify(view, times(1)).hideProgress();
    }

      @Test
      public void shouldNotReturnNullModelSpinnerList() throws IOException {
          presenter = new SelectPaymentMethodPresenter(view, new SelectPaymentMethodModel());
          List<PaymentMethod> paymentMethodList = mockLisPaymentMethod();
          assertNotNull(presenter.loadSpinnerModel(paymentMethodList));
      }

    private ErrorResponse getErrorResponse()  throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String itemError= "{\n" +
                "  \"message\": \"invalid parameters\",\n" +
                "  \"error\": \"bad_request\",\n" +
                "  \"status\": 400,\n" +
                "  \"cause\": [\n" +
                "    {\n" +
                "      \"code\": \"2002\",\n" +
                "      \"description\": \"payment method not found\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        return mapper.readValue(itemError, ErrorResponse.class);
    }

    private List<PaymentMethod> mockLisPaymentMethod()  throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String itemCard= "  {\n" +
                "    \"id\": \"visa\",\n" +
                "    \"name\": \"Visa\",\n" +
                "    \"paymentTypeId\": \"credit_card\",\n" +
                "    \"status\": \"active\",\n" +
                "    \"secureThumbnail\": \"https://www.mercadopago.com/org-img/MP3/API/logos/visa.gif\",\n" +
                "    \"thumbnail\": \"http://img.mlstatic.com/org-img/MP3/API/logos/visa.gif\",\n" +
                "    \"deferredCapture\": \"supported\",\n" +
                "    \"settings\": [\n" +
                "      {\n" +
                "        \"securityCode\": {\n" +
                "          \"mode\": \"mandatory\",\n" +
                "          \"cardLocation\": \"back\",\n" +
                "          \"length\": 3\n" +
                "        },\n" +
                "        \"cardNumber\": {\n" +
                "          \"length\": 16,\n" +
                "          \"validation\": \"standard\"\n" +
                "        },\n" +
                "        \"bin\": {\n" +
                "          \"pattern\": \"^4\",\n" +
                "          \"installmentsPattern\": \"^4\",\n" +
                "          \"exclusionPattern\": \"^(443264|400276|400615|402914|434543|416679|404625|405069|405515|405516|405755|405896|405897|406290|406291|406375|406652|406998|406999|408515|410082|410083|410121|410123|410853|411849|417309|421738|423623|428062|428063|428064|434795|437996|439818|442371|442548|444060|446343|446344|446347|450412|450799|451377|451701|451751|451756|451757|451758|451761|451763|451764|451765|451766|451767|451768|451769|451770|451772|451773|457596|457665|462815|463465|468508|473710|473711|473712|473714|473715|473716|473717|473718|473719|473720|473721|473722|473725|477051|477053|481397|481501|481502|481550|483002|483020|483188|489412|492528|499859|446344|446345|446346|400448)\"\n" +
                "        }\n" +
                "      }\n" +
                "    ],\n" +
                "    \"additionalInfoNeeded\": [\n" +
                "      \"cardholderName\",\n" +
                "      \"cardholderIdentificationType\",\n" +
                "      \"cardholderIdentificationNumber\"\n" +
                "    ],\n" +
                "    \"minAllowedAmount\": 2,\n" +
                "    \"maxAllowedAmount\": 250000,\n" +
                "    \"accreditationTime\": 2880,\n" +
                "    \"financialInstitutions\": [\n" +
                "    ],\n" +
                "    \"processingModes\": [\n" +
                "      \"aggregator\"\n" +
                "    ]\n" +
                "  }";
        PaymentMethod card = mapper.readValue(itemCard, PaymentMethod.class);
        List<PaymentMethod> paymentMethodList = new ArrayList<>();
        paymentMethodList.add(card);

        return paymentMethodList;
    }

}
