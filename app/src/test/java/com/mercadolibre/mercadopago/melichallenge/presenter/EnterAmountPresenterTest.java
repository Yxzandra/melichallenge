package com.mercadolibre.mercadopago.melichallenge.presenter;


import android.content.Context;

import com.mercadolibre.mercadopago.melichallenge.BuildConfig;
import com.mercadolibre.mercadopago.melichallenge.R;
import com.mercadolibre.mercadopago.melichallenge.helpers.CustomMessage;
import com.mercadolibre.mercadopago.melichallenge.mvp.view.MainActivity;
import com.mercadolibre.mercadopago.melichallenge.mvp.view.EnterAmountFragment;
import com.mercadolibre.mercadopago.melichallenge.mvp.presenter.EnterAmountPresenter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)

public class EnterAmountPresenterTest {
    private EnterAmountFragment view;
    private EnterAmountPresenter presenter;
    private Context context;


    @Before
    public void setUp() {
        view = mock(EnterAmountFragment.class);
        context = mock(MainActivity.class);
    }

    @Test
    public void shouldShowErrorMessageOnEmptyAmount(){
        when(context.getString(R.string.amount_empty)).thenReturn("Su monto no puede estar vacío");
        presenter = new EnterAmountPresenter(view);
        presenter.validateAmount("",context);
        verify(view, times(1)).showError(CustomMessage.CODE_100_INFORMATION ,"Su monto no puede estar vacío");
    }

    @Test
    public void shouldShowErrorMessageOnInvalidAmountInterval(){
        when(context.getString(R.string.amount_error)).thenReturn("Su monto no puede ser menor de 0 ni mayor de 250000");

        presenter = new EnterAmountPresenter(view);
        presenter.validateAmount("2500000",context);
        verify(view, times(1)).showError(CustomMessage.CODE_100_INFORMATION,"Su monto no puede ser menor de 0 ni mayor de 250000");
    }

    @Test
    public void shouldShowViewOnValidAmount(){
        presenter = new EnterAmountPresenter(view);
        presenter.validateAmount("100",context);

        verify(view, times(1)).showView();
    }


}
